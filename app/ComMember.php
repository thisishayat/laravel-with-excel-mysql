<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ComMember extends Model
{
    //
    protected $table = "com_members";
    protected $fillable = [
        'cc_name',
        'slum_name',
        'household_id',
        'monthly_income',
        'member_name',
        'com_memb_id',
        'nid',
        'occupation',
        'occupation_type',
        'rel_with_household',
    ];


}
