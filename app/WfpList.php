<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WfpList extends Model
{
    //
    protected $table = "wfp_lists";

    protected $fillable = [
        'slum_name',
        'ward',
        'thana',
        'household_id',
        'mem_in_fam',
        'wfp_id'
    ];

}
