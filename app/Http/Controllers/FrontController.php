<?php
/**
 * Created by PhpStorm.
 * User: backend
 * Date: 5/26/18
 * Time: 4:31 PM
 */

namespace App\Http\Controllers;


use App\ComMember;
use App\Country;
use App\Covid;
use App\WfpList;
use App\WfpMainReport;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Maatwebsite\Excel\Facades\Excel;
use phpDocumentor\Reflection\Types\Null_;

class FrontController
{
    public function test(){
//        http://localhost:8000/excel-file/country list.xls
//        // covid
//        $resExc = Excel::load('excel-file/finalv2.xls', function($reader) {
//
//            // Getting all results
//            $results = $reader->all();
//
//        });
//
////        $resExc = ['asd'=>125];
//        $resExc = $resExc->toArray();
//        foreach ($resExc as $r){
//
//            $chek = Covid::where('household_id',$r['household_household_id'])->get()->toArray();
//            if(!$chek){
//                $hidInsert = Covid::create([
//                    "household_id"=>$r['household_household_id'],
//                    "household_head_name"=>$r['head_of_household_name'],
//                    "mobile_no"=>$r['mobile_no.'],
//                    "bkash_no"=>$r['bkash_number'],
//                    "bkash_yes_no"=>$r['do_you_have_bkash_number'],
//                    "status"=>1,
//                ]);
//            }
//
//        }
//
//        return $r;

        //dd($resExc->toArray());


        // wfp
//        $resExc = Excel::load('excel-file/wfplistv2.xls', function($reader) {
//
//            // Getting all results
//            $results = $reader->all();
//
//        });
//
////        $resExc = ['asd'=>125];
//        $resExc = $resExc->toArray();
//
//        foreach ($resExc as $r){
//
//            $chek = WfpList::where('household_id',$r['household_id'])->get()->toArray();
//            if(!$chek){
//                $hidInsert = WfpList::create([
//                    "slum_name"=>$r['slumcolony_name'],
//                    "ward"=>$r['ward_no.'],
//                    "thana"=>$r['thana'],
//                    "household_id"=>$r['household_id'],
//                    "mem_in_fam"=>$r['total_no._of_the_members_in_the_hh'],
//                    "wfp_id"=>$r['wfpid'],
//                ]);
//            }
//
//        }
//
//        return $r;
//
        // community member report
//        $resExc = Excel::load('excel-file/commv2.xls', function($reader) {
//
//            // Getting all results
//            $results = $reader->all();
//
//        });
//
////        $resExc = ['asd'=>125];
//        $resExc = $resExc->toArray();
//
////        dd($resExc);
//
//        foreach ($resExc as $r){
//
//            $chek = ComMember::where('household_id',$r['household_id'])->get()->toArray();
//            if(!$chek){
//                $hidInsert = ComMember::create([
//                    'cc_name'=>$r['city_corporation_municipality'],
//                    'slum_name'=>$r['slumcolony_name'],
//                    'household_id'=>$r['household_id'],
//                    'member_name'=>$r['community_member_name'],
//                    'com_memb_id'=>$r['community_members_id'],
//                    'monthly_income'=>$r['monthly_income'],
//                    'nid'=>$r['national_id_card_no.'],
//                    'occupation'=>$r['occupation'],
//                    'occupation_type'=>$r['occupation_type'],
//                    'rel_with_household'=>$r['relation_with_head_of_household'],
//                ]);
//            }
//
//        }
//
//        return $r;


//        $resExc = WfpList::all()->toArray();
//        foreach ($resExc as $r){
//            $chek = WfpMainReport::where('household_id',$r['household_id'])->get()->toArray();
//            if(!$chek){
//                $repInsert = WfpMainReport::create([
//                    "ward"=>$r['ward'],
//                    "thana"=>$r['thana'],
//                    "household_id"=>$r['household_id'],
//                    "mem_in_fam"=>$r['mem_in_fam'],
//                    "wfp_id"=>$r['wfp_id'],
//                    'slum_name'=>$r['slum_name'],
//                    ]);
//
//
//                $checkCov = Covid::where('household_id',$r['household_id'])->get()->toArray();
//                if(count($checkCov) > 0){
//                    $checkCov = $checkCov[0];
//                    $update = WfpMainReport::where('id',$repInsert->id)->update([
//                        "cov_household_id"=>$checkCov['household_id'],
//                        "household_head_name"=>$checkCov['household_head_name'],
//                        "mobile_no"=>$checkCov['mobile_no'],
//                        "bkash_no"=>$checkCov['bkash_no'],
//                        "bkash_yes_no"=>$checkCov['bkash_yes_no']
//                    ]);
//                }
//
//                $checkCom = ComMember::where('household_id',$r['household_id'])->get()->toArray();
//
//                if(count($checkCom) > 0){
//                    $checkCom = $checkCom[0];
//                    $update = WfpMainReport::where('id',$repInsert->id)->update([
//                        'cc_name'=>$checkCom['cc_name'],
//                        'com_household_id'=>$checkCom['household_id'],
//                        'member_name'=>$checkCom['member_name'],
//                        'com_memb_id'=>$checkCom['com_memb_id'],
//                        'monthly_income'=>$checkCom['monthly_income'],
//                        'nid'=>$checkCom['nid'],
//                        'occupation'=>$checkCom['occupation'],
//                        'occupation_type'=>$checkCom['occupation_type'],
//                        'rel_with_household'=>$checkCom['rel_with_household'],
//
//                    ]);
//                }
//
//            }
//
//        }

//        return $r;


//        // a excel file insert with data

//        $file_location = 'excel-file/comm_list.xls';
//        $table_name = "comm_list";
//        $uniq = 'com_mem_id';
//
//        $resExc = Excel::load($file_location, function($reader) {
//            // Getting all results
//            $results = $reader->all();
//        });
//        $resExc = $resExc->toArray();
////        dd($resExc);
//
//
//        $fileFields= Excel::selectSheetsByIndex(0)->load($file_location)->get();
//        $fileFields= $fileFields->getHeading();
////        dd($fileFields);
//
//            // check if table is not already exists
//            if (!Schema::hasTable($table_name)) {
//                Schema::create($table_name, function (Blueprint $table) use ($fileFields, $table_name)
//                {
//                    $count = 0;
//                    $table->increments('id');
//                    if (count($fileFields) > 0) {
//                        foreach ($fileFields as $field) {
//                            if($field != ""){
//                                $table->{'text'}($field)->nullable();
//                            }
//
//                        }
//                    }
//                });
//
//            }
//           // dd('table ok');
////
//        $resExc = Excel::load($file_location, function($reader) {
//            // Getting all results
//            $results = $reader->all();
//        });
//        $resExc = $resExc->toArray();
////        dd($resExc);
//        $clmNameSts = 1;
//        $columnNo = count($resExc[0]);
//        foreach ($resExc as $r){
//            if($clmNameSts){
//                foreach ($r as $key=>$value){
//                   $columnNames[] = $key;
//                }
//                $clmNameSts = 0;
//            }
//            $chek = DB::table($table_name)->where($uniq,$r[$uniq])->get()->toArray();
//            if(!$chek){
//                $hidInsert =   DB::table($table_name)->insert([
//                    $columnNames[$columnNo-1]=>$r[$columnNames[$columnNo-1]],
//                    $columnNames[$columnNo-2]=>$r[$columnNames[$columnNo-2]],
//                    $columnNames[$columnNo-3]=>$r[$columnNames[$columnNo-3]],
//                    $columnNames[$columnNo-4]=>$r[$columnNames[$columnNo-4]],
//                    $columnNames[$columnNo-5]=>$r[$columnNames[$columnNo-5]],
//                    $columnNames[$columnNo-6]=>$r[$columnNames[$columnNo-6]],
//                    $columnNames[$columnNo-7]=>$r[$columnNames[$columnNo-7]],
//                    $columnNames[$columnNo-8]=>$r[$columnNames[$columnNo-8]],
//                    $columnNames[$columnNo-9]=>$r[$columnNames[$columnNo-9]],
//                    $columnNames[$columnNo-10]=>$r[$columnNames[$columnNo-10]],
////                    $columnNames[$columnNo-11]=>$r[$columnNames[$columnNo-11]],
////                    $columnNames[$columnNo-12]=>$r[$columnNames[$columnNo-12]],
////                    $columnNames[$columnNo-13]=>$r[$columnNames[$columnNo-13]]
//                    ]
//                );
//
//            }
//
//        }
//        dump($r);
//        dd('all data insert done');


//
////        // data download as excel
        $chek = DB::table('comm_list')
            ->where('slum_name','Shampur')
//            ->where('hh_type','Poor')
            ->where('hh_type','Extreme Poor')
            ->where('rel_hh_household','Head of Household')
            ->get()->toArray();
//        dd(count($chek));
        $total_arr=[];
//        $list_name = 'Kollayanpur Poor Household';
        $list_name = 'Shampur Extreme Poor Households';

        $update = DB::table('comm_list')->update(['status'=>0]);

        foreach ($chek as $c){
            $exist_in_main_spouse = DB::table('comm_list')->where('hh_id',$c->hh_id)->where('rel_hh_household','Spouse')->get()->toArray();
            // duplicate check
//            if(count($exist_in_main) > 1) {
//                //dump($c);
//                // duplicate check
//                foreach ($exist_in_main as $duplicate) {
//                    $arr[$list_name . ' serial no.'] = $duplicate->serial_no;
//                    $arr[$list_name . ' gfems id'] = $duplicate->gfeme_id;
//                    $arr[$list_name . ' slum name'] = $duplicate->slum_name;
//                    $arr['HH ID'] = $duplicate->hh_id;
//                    $arr['Member name'] = $duplicate->member_name;
//                    $arr['Bkash number'] = $duplicate->bkash_num_ber;
//                    $arr['Created by'] = $duplicate->created_by;
//                    $arr['Create date'] = $duplicate->create_date;
//                    $total_arr[] = $arr;
//
//                }
//              $update =  $chek = DB::table('gfem_06_sep')->where('bkash_num_ber',$c->bkash_num_ber)->update(['status'=>1]);
//
//            }



                $exist_in_main = $c;
                $arr['City Corporation / Municipality']=$exist_in_main->cc_name;
                $arr['Household ID']=$exist_in_main->hh_id;
                $arr['Community Member Name']=$exist_in_main->comm_mem;

                if(count($exist_in_main_spouse) > 0){

                    $exist_in_main_spouse = $exist_in_main_spouse[0];
                    $arr['Spouse Name']=$exist_in_main_spouse->comm_mem;
                }

                $arr['Community Member\'s ID']=$exist_in_main->com_mem_id                ;
                $arr['Occupation']=$c->occupation;
                $arr['Relation with Head of Household']=$c->rel_hh_household;
                $arr['Contact Number 1']=$c->contact_no;
                $arr['Contact Number 2']=$c->phone_no;
                $arr['Slum Name']=$c->slum_name;
                $arr['HH type']=$c->hh_type;
                $total_arr[] = $arr;


            $update = DB::table('comm_list')->where('hh_id',$c->hh_id)->update(['status'=>1]);

        }
//        dd($total_arr);
        $excel_download = Excel::create($list_name, function($excel) use ($total_arr) {

            $excel->sheet('Sheetname', function($sheet) use ($total_arr) {

                $sheet->fromArray($total_arr);

            });

        })->export('xls');

       return $excel_download;



    }
}