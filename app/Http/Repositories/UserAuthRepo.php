<?php
/**
 * Created by PhpStorm.
 * User: backend
 * Date: 2/19/18
 * Time: 4:12 PM
 */
namespace App\Http\Repositories;


use App\Country;
use App\EmailTokenDtl;
use App\Helper\Helper;
use App\MobileSignupTokens;
use App\PhoneCodeDetails;
use App\User;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class UserAuthRepo
{

    /**
     *
     * @param $postVars
     * @return array|int|string
     */
    public function userSignp($request)
    {
        $status = [
            'status' => trans('custom.status.validError'),
            'msg' => trans('auth.errors.dataInsertFail')
        ];
        $postVars = $request->all();
        $validator = Validator::make($postVars, [
            'mobile' => 'required',
            'country_name' => 'required|string|exists:countries,country_name',
            'phone_code' => 'required|string|exists:countries,phone_code',
            'country_code' => 'required|string',
        ]);

        if ($validator != '' && $validator->fails()) {
            $status = [
                'status' => trans('custom.status.validError'),
                'msg' => trans('auth.errors.dataInsertFail'),
                'data' => $validator->messages()
            ];
            return $status;
        } else {

            $MobileSignUpToken = new MobileSignupTokens();
            $tokenChk = $MobileSignUpToken
                ->where('expiry_time', '>', \Carbon\Carbon::now())
                ->where('phone', $postVars['mobile'])->get();
            if (count($tokenChk) > 0) {
                $status = trans('custom.status.noData');
                return ['status' => $status, 'data' => ['msg' => trans('auth.activation_resned.sendPhoneCodeAlready')]];
            }
            $sendCode = $this->createTokenForMobileSignUp($postVars);
            if ($sendCode['status']) {
                $sendCode['data']['attrs'] = unserialize($sendCode['data']['attrs']);
                $status = [
                    'status' => trans('custom.status.success'),
                    'data' => $sendCode['data'],
                    'msg' => trans('auth.activation_resned.sendPhoneCode')
                ];
            }
            return $status;

        }
    }

    public function webUserSignp($request)
    {
        $status = [
            'status' => trans('custom.status.validError'),
            'msg' => trans('auth.errors.dataInsertFail')
        ];
        $params = $request->all();
        $validator = Validator::make($params, [
            'mobile' => 'required|string|unique:users,mobile',
            'device_no' => 'sometimes|string',
            'push_key' => 'sometimes|string',
            'country_code' => 'required|exists:countries,country_code',

            'first_name' => 'required|string',
            'middle_name' => 'sometimes|string',
            'email' => 'required|email|unique:users,email',
            'last_name' => 'required|string',
            'password' => 'required|string',
            'gender' => 'required|regex:/^[1-3]$/',
        ]);

        if ($validator != '' && $validator->fails()) {
            $errors = $validator->errors()->toArray();
            $errorArr = array_flatten($errors);
            $status = [
                'status' => trans('custom.status.validError'),
                'msg' => $errorArr[0],
                'error' => $errors

            ];
            return $status;
        } else {
            $tokenChk = EmailTokenDtl::
            where('expire_at', '>', \Carbon\Carbon::now()->toDateTimeString())
                ->where('email', $request->get('email'))
                ->orderBy('id', 'desc')
                ->take(1)
                ->get()
                ->toArray();
            if (count($tokenChk) > 0) {
                $status = [
                    'status' => trans('custom.status.success'),
                    'msg' => trans('custom.msg.emailTokenSend')
                ];
            } else {
                $params['password'] = bcrypt($params['password']);
                $emailDtls = EmailTokenDtl::create([
                    'token' => strtoupper(uniqid() . time() . uniqid()),
                    'email' => $request->get('email'),
                    'expire_at' => date('Y-m-d H:i:s', time() + 60 * 5), // mobile token signup,
                    'signup_details' => serialize($params),
                ]);

                $emailData['token'] = $emailDtls->token;
                $emailData['email'] = $emailDtls->email;
                $emailData['title'] = "Travel Home sign up";
                $userMailSend = Mail::send('email.sign-up-mail', ['emdailData' => $emailData], function ($message) use ($emailData) {
                    $message->from(env('MAIL_FROM'), 'Travel Home');
                    $message->to($emailData['email']);
                    $message->subject($emailData['title']);
                });

                DB::commit();
                $status = [
                    'status' => trans('custom.status.success'),
                    'msg' => trans('custom.msg.signUpSuccess')
                ];
            }


            return $status;

        }
    }

    public function webUserSignpVerfiy($request)
    {
        $status = [
            'status' => trans('custom.status.validError'),
            'msg' => trans('auth.errors.dataInsertFail')
        ];
        $params = $request->all();
        $validator = Validator::make($params, [
            'token' => 'required|string|exists:email_token_dtls,token',
        ]);

        if ($validator != '' && $validator->fails()) {
            $status = [
                'status' => trans('custom.status.validError'),
                'msg' => trans('custom.msg.validationError'),
                'data' => $validator->messages()
            ];
            return $status;
        } else {

            try {
                DB::beginTransaction();

                $tokenChk = EmailTokenDtl::
                where('expire_at', '>', \Carbon\Carbon::now()->toDateTimeString())
                    ->where('token', $request->get('token'))
                    ->orderBy('id', 'desc')
                    ->take(1)
                    ->get()
                    ->toArray();
                if (count($tokenChk) > 0) {
                    $tokenChk = $tokenChk[0];
                    $params = unserialize($tokenChk['signup_details']);

                    $validator = Validator::make($params, [
                        'mobile' => 'required|string|unique:users,mobile',
                        'device_no' => 'sometimes|string',
                        'push_key' => 'sometimes|string',
                        'country_code' => 'required|exists:countries,country_code',

                        'first_name' => 'required|string',
                        'middle_name' => 'sometimes|string',
                        'email' => 'required|email|unique:users,email',
                        'last_name' => 'required|string',
                        'password' => 'required|string',
                        'gender' => 'required|regex:/^[1-3]$/',
                    ]);

                    if ($validator != '' && $validator->fails()) {
                        $status = [
                            'status' => trans('custom.status.validError'),
                            'msg' => trans('custom.msg.validationError'),
                            'data' => $validator->messages()
                        ];
                        return $status;
                    }

                    $user = User::create([
                        'mobile' => $params['mobile'],
                        'system_id' => strtoupper(md5(rand(11111111, 99999999) . uniqid() . time())),
                        'first_name' => $params['first_name'],
                        'last_name' => $params['last_name'],
                        'email' => $params['email'],
                        'gender' => $params['gender'],
                        'password' => $params['password'],
                        'country_code' => strtoupper($params['country_code']),
                        'avatar' => "default.png",
                        'device_id' => isset($params['device_id']) ? $params['device_id'] : null,
                        'push_not_key' => isset($params['push_not_key']) ? $params['push_not_key'] : null,
                        'api_token' => md5(time() . rand(11111111, 99999999) . uniqid() . time()),
                        'is_active' => 1,
                    ]);

                    $tokenUpdate = EmailTokenDtl::where('token', $request->get('token'))->update([
                        'expire_at' => Carbon::now()
                    ]);

                    DB::commit();
                    $status = [
                        'status' => trans('custom.status.success'),
                        'msg' => trans('custom.msg.signUpSuccess')
                    ];
                } else {

                    $status = [
                        'status' => trans('custom.status.expectFailed'),
                        'msg' => trans('custom.msg.tokenExpire')
                    ];

                }
            } catch (\Exception $e) {
                DB::rollBack();
                $helper = new Helper();
                $status = [
                    'status' => trans('custom.status.expectFailed'),
                    'msg' => trans('custom.msg.invalid'),
                    'error' => $helper->getErrors($e)
                ];
            }
            return $status;

        }
    }


    public function mobileLogIn($request)
    {


        try {
            $input = $request->all();

            $email = $request->get('username');

            if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $input['email'] = $request->get('username');
                $reqEmail = 'required|exists:users,email';
                $reqMobile = '';
                $inputName = 'email';
            } else {
                $input['mobile'] = $request->get('username');
                $reqMobile = 'required|exists:users,mobile';
                $reqEmail = '';
                $inputName = 'mobile';
            }
            $validator = Validator::make($input, [
                'email' => $reqEmail,
                'mobile' => $reqMobile,
                'password' => 'required|string',
            ]);
            if ($validator->fails()) {
                $errors = $validator->errors()->toArray();
                $errorArr = array_flatten($errors);
                $res = [
                    'status' => trans('custom.status.validError'),
                    'msg' => $errorArr[0],
                    'error' => $errors

                ];
                return $res;

            }
            $credentials = array(
                $inputName => $request->get('username'),
                'password' => $input['password'],
                'is_active' => 1,
                'role' => 2,
            );

            $remember = isset($input['remember']) ? $input['remember'] : false;

            $res = [
                'status' => trans('custom.status.failed'),
                'msg' => trans('custom.msg.invalidCred'),
            ];
            if (Auth::attempt($credentials, $remember)) {
                $helper = new Helper();
                $userData = $helper->getSingleUserInfoById(Auth::id());
                $res = [
                    'status' => trans('custom.status.success'),
                    'msg' => trans('custom.msg.dataGet'),
                    'data' => $userData,
                ];
            }
        } catch (Exception $e) {
            $helper = new Helper();
            $res = [
                'status' => trans('custom.status.expectFailed'),
                'msg' => trans('custom.msg.invalid'),
                'error' => $helper->getErrors($e)
            ];
        }
        return $res;

    }

    public function webLogIn($request)
    {
        try {
            $input = $request->all();
            $email = $request->get('username');

            if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $input['email'] = $request->get('username');
                $reqEmail = 'required|exists:users,email';
                $reqMobile = '';
                $inputName = 'email';
            } else {
                $input['mobile'] = $request->get('username');
                $reqMobile = 'required|exists:users,mobile';
                $reqEmail = '';
                $inputName = 'mobile';
            }
            $validator = Validator::make($input, [
                'email' => $reqEmail,
                'mobile' => $reqMobile,
                'password' => 'required|string',
            ]);
            if ($validator->fails()) {
                $errors = $validator->errors()->toArray();
                $errorArr = array_flatten($errors);
                $res = [
                    'status' => trans('custom.status.validError'),
                    'msg' => $errorArr[0],
                    'error' => $errors

                ];
                return $res;
            }

            $credentials = array(
                $inputName => $request->get('username'),
                'password' => $input['password'],
                'is_active' => 1,
                'role' => 2,
            );

            $remember = isset($input['remember']) ? $input['remember'] : false;

            $res = [
                'status' => trans('custom.status.failed'),
                'msg' => trans('custom.msg.invalidCred'),
            ];
            if (Auth::attempt($credentials, $remember)) {
                $helper = new Helper();
                $userData = $helper->getSingleUserInfoById(Auth::id());
                $res = [
                    'status' => trans('custom.status.success'),
                    'msg' => trans('custom.msg.dataGet'),
                    'data' => $userData,
                ];
            }
        } catch (Exception $e) {
            $helper = new Helper();
            $res = [
                'status' => trans('custom.status.expectFailed'),
                'msg' => trans('custom.msg.invalid'),
                'error' => $helper->getErrors($e)
            ];
        }
        return $res;

    }


    /**
     * @param $postVars
     * @return array
     */
    function createTokenForMobileSignUp($postVars)
    {
        $operator = 1;
        $balance_chk = "http://107.20.199.106/restapi/account/1/balance";
        $auth = ['username' => env('MOB_USER'), 'password' => env('MOB_PASS')];
        $helper = new Helper();
        $parse_balance = $helper->curlReqJsonBasicAuth($balance_chk, "GET", $auth, []);
        //$parse_balance = '{"balance":0.000000,"currency":"BDT"}';
        $balance = json_decode($parse_balance, true);
        $country = new Country();
        $codeDtls = new phoneCodeDetails();
        $cntryName = $country->where('country_name', $postVars['country_name'])->first();
        $balanceAmnt = 00.00;
        if (count($balance) > 1) {
            $balanceAmnt = $balance['balance'];
            //$balanceAmnt = 100;
            // send email notification if balance is low
            try {
                if ($balanceAmnt < 40) {
                    $title = "Travel Home SMS Balance Notification";
                    $content['message'] = "SMS balance is running low.Please check and recharge immediately.";
                    $content['balance'] = $balanceAmnt;
                    $content['avatar'] = "default.png";
                    $content['fullName'] = "Travel Home";
                    $emailsCollection = ['tapash@tgalimited.com', 'hayat@tgalimited.com'];

                    $i = 0;
                    foreach ($emailsCollection as $email) {
                        $content['email'] = $email;
                        $i = 1;
                        Mail::send('email.sms-balance', ['title' => $title, 'content' => $content], function ($message) use ($email) {
                            $message->from(env('MAIL_FROM'), 'Travel Home');
                            $message->to($email);
                            $message->cc(['hayat@tgalimited.com']);
                            $message->subject('Travel Home SMS Balance Notification');
                        });
                    }
                }
            } catch (\Exception $e) {
                return false;
            }
        }
        if (count($cntryName) > 0) {
            DB::beginTransaction();
            try {
                // save data to mobile_tokens
                $MobileCodeSignUpRepo = new MobileSignupTokens();
                $MobileCodeSignUpRepo->code = mt_rand(100000, 999999);
                $MobileCodeSignUpRepo->expiry_time = date('Y-m-d H:i:s', time() + 60 * 1); // mobile token signup
                $MobileCodeSignUpRepo->phone = $postVars['mobile'];
                $MobileCodeSignUpRepo->signup_status = 0;
                $MobileCodeSignUpRepo->attrs = serialize($postVars);
                $MobileCodeSignUpRepo->save();


                // insert data to phone_code_details table
                $codeDtls->country_id = $cntryName['id'];
                $codeDtls->country = $postVars['country_name'];
                $codeDtls->operator = $operator;
                $codeDtls->destination = $postVars['mobile'];
                $codeDtls->sms_code = $MobileCodeSignUpRepo->code;
                $codeDtls->current_balance = $balanceAmnt;
                $codeDtls->sms_send = 0;
                $codeDtls->created_at = Carbon::now();
                $codeDtls->save();

                $sendToMobParam = [
                    'country_code' => $postVars['country_code'],
                    'country_name' => $postVars['country_name'],
                    'token' => $MobileCodeSignUpRepo->code,
                    'phone' => $postVars['mobile'],
                    'code_id' => $codeDtls->id,
                    'msg' => $MobileCodeSignUpRepo->code . ' is your Travel Home confirmation code.'

                ];
                $this->sendSmsPhoneInfobip($sendToMobParam);
                DB::commit();
                return ['status' => true, 'data' => $MobileCodeSignUpRepo->toArray()];
            } catch (\Exception $e) {
                dump($e);
                DB::rollback();
                return ['status' => false];
            }
        }
        return ['status' => false];
    }

    /**
     * @param array $param
     * @return int
     */
    public function sendSmsPhoneInfobip($param = array())
    {
        //====================================== send SMS to the user
        $phoneCodeDetails = new phoneCodeDetails();
        try {
            $hasPlus = strpos($param['phone'], '+');
            $phone = ($hasPlus === false) ? $param['phone'] : substr($param['phone'], 1);
            $msg = $param['msg'];
            $helper = new Helper();
            $auth = ['username' => env('MOB_USER'), 'password' => env('MOB_PASS')];
            $live_url = "http://107.20.199.106/restapi/sms/1/text/single";
            $data = [
                'from' => 'TGA Limited',
                // 'from'=>'8804445650000',
                'to' => "$phone",
                'text' => $msg

            ];
            $parse_url = $helper->curlReqJsonBasicAuth($live_url, "POST", $auth, $data);
            //$parse_url = '{"messages":[{"to":"8801991126415","status":{"groupId": 1,"groupName": "PENDING","id": 7,"name": "PENDING_ENROUTE","description": "Message sent to next instance"},"smsCount": 1,"messageId": "2101149718951631161"}]}';
            //$parse_url = '{"messages":[{"status":{"groupId":5,"groupName":"REJECTED","id":51,"name":"MISSING_TO","description":"Missing destination.","action":"Check to parameter."}}]}';

            $response_msg = json_decode($parse_url, true);
            $statusGroupId = $response_msg['messages'][0]['status']['groupId'];

            $sms_send = 0;
            $status = 0;
            $msgCode = '';
            if ($statusGroupId == 1 || $statusGroupId == 3) { // pending/delivered
                $sms_send = 1;
                $status = 1;
                $msgCode = $response_msg['messages'][0]['messageId'];
            }
            // update sms send status and datetime
            $updateCodeDtls = $phoneCodeDetails->find($param['code_id']);
            $updateCodeDtls->sms_send = $sms_send;
            $updateCodeDtls->sms_send_at = Carbon::now();
            $updateCodeDtls->save();
        } catch (\Exception $e) {
            return 0;
        }

        $updateCodeDtls = null;
        try {
            $updateCodeDtls = $phoneCodeDetails->find($param['code_id']);
            $updateCodeDtls->code = $statusGroupId;
            $updateCodeDtls->status = $status;
            $updateCodeDtls->msg_id = $msgCode;
            $updateCodeDtls->response_msg = json_encode($response_msg);
            $updateCodeDtls->response_at = Carbon::now();
            $saveUpdate = $updateCodeDtls->save();
            if ($saveUpdate) {
                return 1;
            }
        } catch (\Exception $e) {
            return 0;
        }
        return 0;
    }

    /**
     * mobile number verify and sign up
     * @param $params
     * @return array|\Illuminate\Contracts\Translation\Translator|null|string
     */
    public function mobileCodeSignUpCodeVerify($params)
    {
        $status = [
            'status' => trans('custom.status.validError'),
            'msg' => trans('auth.codeNotValid')
        ];

        $user = new User();
        $chkNumber = $user->where(['mobile' => isset($params['mobile']) ? $params['mobile'] : 0, "is_active" => 1])->get();
        $validator = Validator::make($params, [
            'id' => 'required',
            'code' => 'required|string|exists:mobile_signup_tokens,code',
            'mobile' => 'required|string|exists:mobile_signup_tokens,phone',
            'device_no' => 'sometimes|string',
            'push_key' => 'sometimes|string',
            'country_code' => 'required|exists:countries,country_code',

            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'password' => 'required|string',
            'gender' => 'required|regex:/^[1-3]$/',

        ], trans('auth.errors.messages'));

        if ($validator != '' && $validator->fails()) {
            $status = [
                'status' => trans('custom.status.validError'),
                'msg' => trans('auth.codeNotValid'),
                'dataInfo' => $validator->messages(),
            ];
            return $status;
        } else {
            $params['country_code'] = strtoupper($params['country_code']);
            $userDtls = $chkNumber->first();
            $MobileSignUpToken = new MobileSignupTokens();
            $tokenChk = $MobileSignUpToken
                ->where('id', $params['id'])
                ->where('expiry_time', '>', \Carbon\Carbon::now()->toDateTimeString())
                ->orderBy('id', 'desc')
                ->get()->toArray();
            if (count($tokenChk) > 0) {
                try {
                    DB::beginTransaction();
                    if (count($chkNumber) > 0) {

                        $userTbl = new User();
                        $user = $userTbl->find($userDtls->id);
                        $user->device_id = isset($params['device_id']) ? $params['device_id'] : null;
                        $user->push_not_key = isset($params['push_not_key']) ? $params['push_not_key'] : null;
                        $user->save();

                        $success = $MobileSignUpToken
                            ->where('id', $params['id'])
                            ->update([
                                'signup_status' => 1,
                                'type' => 2,
                                'user_id' => $userDtls->id,
                                'expiry_time' => Carbon::now()
                            ]);
                        $userId = $userDtls->id;

                    } else {
                        $user = User::create([
                            'mobile' => $params['mobile'],
                            'system_id' => strtoupper(md5(rand(11111111, 99999999) . uniqid() . time())),
                            'first_name' => $params['first_name'],
                            'last_name' => $params['last_name'],
                            'gender' => $params['gender'],
                            'password' => bcrypt($params['password']),
                            'country_code' => strtoupper($params['country_code']),
                            'avatar' => "default.png",
                            'device_id' => isset($params['device_id']) ? $params['device_id'] : null,
                            'push_not_key' => isset($params['push_not_key']) ? $params['push_not_key'] : null,
                            'api_token' => md5(time() . rand(11111111, 99999999) . uniqid() . time()),
                            'is_active' => 1,
                        ]);
                        $success = $MobileSignUpToken
                            ->where('id', $params['id'])
                            ->update([
                                'signup_status' => 1,
                                'type' => 1,
                                'user_id' => $user->id,
                                'expiry_time' => Carbon::now()
                            ]);
                        $userId = $user->id;

                    }
                    DB::commit();
                    $status = [
                        'status' => trans('custom.status.success'),
                        'data' => $this->getUserInfo(['userId' => $userId]),
                        'msg' => trans('custom.msg.signUpSuccess')
                    ];


                } catch (\Exception $e) {
                    $helper = new Helper();
                    DB::rollBack();
                    $status = [
                        'status' => trans('custom.status.validError'),
                        'msg' => trans('custom.msg.signUpFailed'),
                        'errors' => $helper->getErrors($e)
                    ];
                }

            }
        }
        return $status;
    }

    /**
     * userId
     * get user info
     * @param $param
     * @return array
     */
    public function getUserInfo($param)
    {
        $retData = [];
        $userTbl = new User();
        $get = $userTbl->where(['id' => $param['userId'], 'is_active' => 1])->get();
        if (count($get) > 0) {
            $get = $get->toArray();
            $retData = $get[0];

        }
        return $retData;
    }

    /**
     * token authentications
     * @param $req
     * @return array
     */
    function tokenAuthentication($req)
    {
        $token = "";
        $tmpLoginTbl = new TempLogin();
        if (isset($req['token']) && $req['token'] != '') {
            $token = $req['token'];
        }
        $tokenData = $tmpLoginTbl->select('token', 'user_id')->where('token', $token)->orderBy('id', 'dsc')->get();
        if (count($tokenData) > 0) {
            $tokenData = $tokenData[0];
            $data = $this->getUserInfo(['userId' => $tokenData->user_id]);
            $data['key'] = $token;
            if (count($data) > 0 && isset($data['id'])) {
                return $data;
            }
        }
        return [];
    }


    /**
     * @param $params
     * @return array|string
     */
    public function wrongMobileNumber($params)
    {
        $status = [
            'status' => trans('custom.status.success'),
            'msg' => trans('auth.codeNotValid')
        ];
        $validator = Validator::make($params, [
            'id' => 'required',
            'code' => 'required|string|exists:mobile_signup_tokens,code',
            'phone' => 'required|string|exists:mobile_signup_tokens,phone',
        ], trans('auth.errors.messages'));

        if ($validator != '' && $validator->fails()) {
            $status = [
                'status' => trans('custom.status.validError'),
                'msg' => trans('custom.msg.validationError'),
                'data' => $validator->messages()
            ];
            return $status;

        } else {
            $MobileSignUpToken = new MobileSignupTokens();
            $tokenChk = $MobileSignUpToken
                ->where('id', $params['id'])
                ->where('expiry_time', '>', \Carbon\Carbon::now()->toDateTimeString())
                ->get();
            if (count($tokenChk) > 0) {
                $MobileSignUpToken
                    ->where('id', $params['id'])
                    ->update(['expiry_time' => Carbon::now()]);
                $status = [
                    'status' => trans('custom.status.success'),
                    'data' => [
                        'phone' => $params['phone']
                    ],
                    'msg' => trans('auth.wrong_number_code')
                ];
            }
        }
        return $status;
    }


    public function userRoleChange($param)
    {

        try {
            $userTbl = new User();
            $update = $userTbl
                ->where(['id' => $param['userId'], 'is_active' => 1])->update(['user_type' => $param['role_type']]);
            $dataStatus = [
                'status' => trans('custom.code.successReturn'),
                'msg' => trans('custom.user.userTypeUpdate'),
            ];
        } catch (\Exception $e) {

            dump($e);
            $dataStatus = [
                'status' => trans('custom.code.failedReturn'),
                'msg' => trans('custom.user.userTypeUpdateFail'),
            ];

        }
        return $dataStatus;


    }

    public function userLocationUpdate($param)
    {

        try {
            $userTbl = new User();
            $update = $userTbl
                ->where(['id' => $param['userId'], 'is_active' => 1])->update(['lat' => $param['lat'], 'lon' => $param['lon']]);
            $dataStatus = [
                'status' => trans('custom.code.successReturn'),
                'msg' => trans('custom.user.userLocationUpdate'),
            ];
        } catch (\Exception $e) {
            $dataStatus = [
                'status' => trans('custom.code.failedReturn'),
                'msg' => trans('custom.user.userLocationUpdateFail'),
            ];

        }
        return $dataStatus;
    }

    public function updateUserProfileInfo($param)
    {
        try {
            $userTbl = new User();
            $update = $userTbl
                ->where(['id' => $param['userId'], 'is_active' => 1])
                ->update([
                    'full_name' => $param['request_all']->get('full_name'),
                    'email' => $param['request_all']->get('email'),
                    'user_type' => $param['request_all']->get('user_type'),
                    'notif_on_off' => $param['request_all']->get('notif_on_off'),
                    'blood_grp' => $param['request_all']->get('blood_grp'),
                    'gender' => $param['request_all']->get('gender'),
                    'desc' => $param['request_all']->get('desc'),
                ]);
            $userAuthRepo = new UserAuthRepo();
            $getUserData = $userAuthRepo->getUserInfo(['userId' => $param['userId']]);
            $dataStatus = [
                'status' => trans('custom.code.successReturn'),
                'msg' => trans('custom.user.userLocationUpdate'),
                'data' => $getUserData
            ];
        } catch (\Exception $e) {

            dump($e);
            $dataStatus = [
                'status' => trans('custom.code.failedReturn'),
                'msg' => trans('custom.user.userLocationUpdateFail'),
            ];

        }
        return $dataStatus;
    }

    public function getUserLocation($param)
    {

        try {
            $userTbl = new User();
            $get = $userTbl
                ->select('lat', 'lon')
                ->where(['id' => $param['userId'], 'is_active' => 1])
                ->first()->toArray();
            $dataStatus = [
                'status' => trans('custom.code.successReturn'),
                'msg' => trans('custom.user.userLocationget'),
                'data' => $get
            ];
        } catch (\Exception $e) {

            $dataStatus = [
                'status' => trans('custom.code.failedReturn'),
                'msg' => trans('custom.user.userLocationgetFailed'),
            ];

        }
        return $dataStatus;
    }

    public function updateUserProfilePic($param)
    {
        try {
            DB::beginTransaction();
            $files = Input::file('prof_pic');
            $path = '';
            if ($param['request_all']->hasFile('prof_pic')) {

                $destination = '/uploads/images/profile';
                $name = $files->getClientOriginalName();
                $img = explode(".", $name);
                $name = uniqid() . time() . "." . $img[count($img) - 1];
                $sts = $files->move(public_path() . $destination, $name); // uploading file to given path;
                chmod(public_path() . $destination . '/' . $name, 0777);
                //$files->getClientOriginalName();
                $path = env('PROFILE_IMG_PATH') . $name;
                $userTbl = new User();
                $update = $userTbl->where('id', $param['userId'])->update(['prof_pic' => $name]);
                if ($update) {
                    DB::commit();
                }

            }
            $res = [
                'status' => trans('custom.code.successReturn'),
                'msg' => trans('custom.user.uploadSuccess'),
                "data" => ["prof_pic" => $path]
            ];
        } catch (\Exception $e) {
            DB::rollBack();
            $res = [
                'status' => trans('custom.code.failedReturn'),
                'msg' => trans('custom.user.uploadFail'),
            ];

        }
        return $res;
    }


    public function changeMobileNum($param)
    {
        $dataStatus = [
            'status' => trans('custom.code.failedReturn'),
            'msg' => trans('custom.user.phoneNumberChngFail'),
        ];
        try {
            DB::beginTransaction();

            $MobileSignUpToken = new MobileSignupToken();
            $tokenChk = $MobileSignUpToken
                ->where('id', $param['id'])
                ->where('code', $param['code'])
                ->where('phone', $param['new_phone'])
                ->where('expiry_time', '>', \Carbon\Carbon::now()->toDateTimeString())
                ->get();

            if (count($tokenChk) > 0) {

                $userTbl = new User();
                $update = $userTbl
                    ->where(['id' => $param['userId'], 'is_active' => 1])
                    ->update([
                        'phone' => $param['new_phone'],
                    ]);


                $success = $MobileSignUpToken
                    ->where('id', $param['id'])
                    ->update([
                        'signup_status' => 1,
                        'type' => 3,
                        'user_id' => $param['userId'],
                        'expiry_time' => Carbon::now()
                    ]);
                DB::commit();
                $dataStatus = [
                    'status' => trans('custom.code.successReturn'),
                    'msg' => trans('custom.user.phoneNumberChng'),
                ];
            }

        } catch (\Exception $e) {
            DB::rollBack();

        }
        return $dataStatus;
    }

    public function getTokenViaCode($reqeust)
    {
        $retData = [
            'status' => trans('custom.status.validError'),
            'msg' => trans('custom.msg.noData')
        ];

        $tokenChk = User::
        select('api_token')
            ->where('mobile', $reqeust->get('mobile'))
            ->get();
        if (count($tokenChk) > 0) {
            $getUsr = $tokenChk->first()->toArray();
            $retData = [
                'status' => trans('custom.status.success'),
                'msg' => trans('custom.msg.dataGet'),
                'data' => $getUsr,
            ];


        }
        return $retData;
    }


}