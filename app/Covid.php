<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Covid extends Model
{
    protected $table = "covids";

    protected $fillable = [
        'household_id',
        'household_head_name',
        'mobile_no',
        'bkash_no',
        'bkash_yes_no',
        'status'
    ];
}
