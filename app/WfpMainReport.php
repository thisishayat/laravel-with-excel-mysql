<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WfpMainReport extends Model
{
    //

    protected $table = "wfp_main_reports";
    protected $fillable = [
        'cc_name',
        'slum_name',
        'com_household_id',
        'monthly_income',
        'member_name',
        'com_memb_id',
        'nid',
        'occupation',
        'spouse_mem_id',
        'occupation_type',
        'rel_with_household',

        'cov_household_id',
        'household_head_name',
        'mobile_no',
        'bkash_no',
        'bkash_yes_no',
        'status',

        'ward',
        'thana',
        'household_id',
        'mem_in_fam',
        'wfp_id',
    ];

}
