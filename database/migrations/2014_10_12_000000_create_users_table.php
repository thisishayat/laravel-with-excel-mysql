<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('username')->unique();
            $table->string('password')->nullable();
            $table->string('name');
            $table->string('email')->nullable();
            $table->string('fb_email')->nullable();
            $table->string('gplus_email')->nullable();
            $table->string('email_token')->nullable();
            $table->string('api_token', 60)->unique();

            $table->string('avatar')->nullable();

            $table->unsignedInteger('country_id');
            $table->string('otp')->nullable();
            $table->string('otp_code')->nullable();
            $table->dateTime('otp_expire')->nullable();
            $table->tinyInteger('role')->default(2)->comment('1=admin,2=user,3=edtior,4=sub_editor,5=guest_editor and so on');

            $table->tinyInteger('acnt_sts');
            $table->tinyInteger('is_active');
            $table->date('created_day');
            $table->integer('created_month');
            $table->integer('created_year');
            $table->integer('created_year_week_no');
            $table->softDeletes();
            $table->rememberToken();
            $table->timestamps();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
