<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWfpMainReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wfp_main_reports', function (Blueprint $table) {
            $table->increments('id');

            $table->string('cov_household_id')->nullable();
            $table->text('household_head_name')->nullable();
            $table->text('mobile_no')->nullable();
            $table->text('bkash_no')->nullable();
            $table->text('bkash_yes_no')->nullable();

            $table->string('slum_name');
            $table->string('ward');
            $table->string('thana');
            $table->string('household_id')->unique();
            $table->string('mem_in_fam');
            $table->string('wfp_id')->unique();


            $table->string('cc_name')->nullable();
            $table->string('com_household_id')->nullable();
            $table->string('member_name')->nullable();
            $table->string('com_memb_id')->nullable();
            $table->string('monthly_income')->nullable();
            $table->string('nid')->nullable();
            $table->string('occupation')->nullable();
            $table->string('occupation_type')->nullable();
            $table->string('rel_with_household')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wfp_main_reports');
    }
}
