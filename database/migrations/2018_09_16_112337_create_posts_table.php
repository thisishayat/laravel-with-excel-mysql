<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('category_id');
            $table->unsignedBigInteger('post_by');
            $table->tinyInteger('user_role');
            $table->tinyInteger('post_type')->comment('1.photo,2.video,3.audio');
            $table->string('title');
            $table->string('feature_file_path')->nullable();
            $table->tinyInteger('is_active')->default(1);
            $table->tinyInteger('is_hidden')->default(0);
            $table->date('created_day');
            $table->integer('created_month');
            $table->integer('created_year');
            $table->integer('created_year_week_no');
            $table->softDeletes();
            $table->timestamps();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
