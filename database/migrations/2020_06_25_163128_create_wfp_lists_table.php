<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWfpListsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wfp_lists', function (Blueprint $table) {
            $table->increments('id');
            $table->string('slum_name');
            $table->string('ward');
            $table->string('thana');
            $table->string('household_id')->unique();
            $table->string('mem_in_fam');
            $table->string('wfp_id')->unique();
            $table->string('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wfp_lists');
    }
}
