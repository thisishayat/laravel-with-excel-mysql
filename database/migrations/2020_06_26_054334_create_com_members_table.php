<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('com_members', function (Blueprint $table) {
            $table->increments('id');
            $table->string('cc_name');
            $table->string('slum_name');
            $table->string('household_id')->unique();
            $table->string('member_name');
            $table->string('com_memb_id')->unique();
            $table->string('monthly_income');
            $table->string('nid');
            $table->text('occupation');
            $table->string('occupation_type');
            $table->string('rel_with_household');
            $table->string('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('com_members');
    }
}
